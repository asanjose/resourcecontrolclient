###############################################################################
###############################################################################
FROM hsbcn/base-cpuapi:v1.0-3 AS build_base

# VERSION CONTAINER
LABEL gen_version="1.0"
LABEL build_version="0"

USER root


RUN apt-get update && apt-get  install -y yasm
RUN apt-get update && apt-get  install -y curl




###############################################################################
###############################################################################
FROM build_base as build

COPY src /tmp/build/src
COPY resourcecontrolproto/proto /tmp/build/proto
COPY cmake /tmp/cmake
COPY CMakeLists.txt  /tmp/build
RUN cd /tmp/build  &&  cmake -DCMAKE_MODULE_PATH=/tmp/cmake . && make -j7



#-----------------------------------------------------------------
# User Set
#-----------------------------------------------------------------
USER devel
WORKDIR /home/devel

###############################################################################
###############################################################################
FROM ubuntu:18.04 AS publish

RUN mkdir /app
COPY --from=build /usr/local/lib/libgrpc++.so.1 /usr/local/lib/
COPY --from=build /usr/local/lib/libgrpc.so.7 /usr/local/lib/
COPY --from=build /tmp/build/ResourceControlClient  /app



RUN find /app
CMD ["/app/ResourceControlClient"]





