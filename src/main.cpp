#include <exception>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include "helpers/cxxopts.hpp"
#include "log/aixlog.hpp"
#include "helpers/semaphore.h"
#include "grpc/service_synchronous.h"
#include "grpc/grpc_health_client.h"


using namespace std;




int main(int argc, char *argv[])
{
    try
    {

        her::Semaphore semp;

        // Program Params parse and validation
        std::string ProgramName(argv[0]);
        cxxopts::Options options(ProgramName.c_str(), "");
        options.add_options()("u,url", "Manager URL", cxxopts::value<std::string>()->default_value("server_host:50051"))
        ("h,help", "Print help");

        auto result = options.parse(argc, argv);
        if (result.count("help"))
        {
            std::cout << options.help({"", "Group"}) << std::endl;
            exit(0);
        }

        // URL SET
        std::string service_url =result["url"].as<std::string>();

        //log intialization
        AixLog::Log::init<AixLog::SinkCout>(AixLog::Severity::trace, AixLog::Type::normal);

        
       std::shared_ptr<her::HealtClient> sp_client = std::make_shared<her::HealtClient>( service_url );
       sp_client->Start();






    }
    catch (std::exception &e)
    {
        LOG(ERROR) << " Exception: " << e.what() << endl;
    }

    return 0;
}
