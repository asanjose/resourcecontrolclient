#include <fstream>  // std::ifstream
#include <fstream>
#include <iostream>
#include <sstream>  //std::stringstream
#include <vector>
#include "utils.h"

chr::time_point<chr::high_resolution_clock> tic() { return chr::high_resolution_clock::now(); }

double toc(chr::time_point<chr::high_resolution_clock>& start) {
    auto end = chr::high_resolution_clock::now();
    // std::chrono::duration<double, std::milli> ms = end - start;
    chr::duration<double> elapsed_seconds = end - start;
    return std::round(elapsed_seconds.count() * 1000);
}

