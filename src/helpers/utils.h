
#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <chrono>
#include <cmath>
#include <string>

namespace chr = std::chrono;
using timept = chr::time_point<chr::high_resolution_clock>;

chr::time_point<chr::high_resolution_clock> tic();
double toc(chr::time_point<chr::high_resolution_clock>& start);


#endif