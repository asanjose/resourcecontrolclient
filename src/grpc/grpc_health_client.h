#ifndef RESURCE_CONTROL_CLIENT_H
#define RESURCE_CONTROL_CLIENT_H

#include <iostream>
#include <memory>
#include <string>


#include <grpcpp/grpcpp.h>
#include "healt.grpc.pb.h"


namespace her
{
    using grpc::Channel;
    using grpc::ClientContext;
    using grpc::Status;
    using health::ResourcesItem;
    using health::HealthCheckResponse;
    using health::HealthCheckRequest;
    
    class HealtClient{
    public:
        HealtClient(const std::string url ): m_url(url),
            stub_(health::Health::NewStub(grpc::CreateChannel(url, grpc::InsecureChannelCredentials()))){};
        
        void Start();
        
    private:
        std::unique_ptr<health::Health::Stub> stub_;
        std::string m_url;
        
    };

}  // her Namespace
#endif