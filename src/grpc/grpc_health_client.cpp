#include "grpc_health_client.h"
#include "log/aixlog.hpp"

#include <iostream>
#include <chrono>
#include <thread>
#include <unistd.h>
#include <limits.h>

namespace her
{

     void HealtClient::Start()
     {
         LOG(INFO) << "HealtClient::Start()" << std::endl;

        while(true)
        {
            HealthCheckRequest req;
            HealthCheckResponse reply;
            ClientContext ctx;
            char hostname[HOST_NAME_MAX];
            gethostname(hostname, HOST_NAME_MAX);

            req.set_node_name(hostname);

            auto item = req.add_items();
            item->set_key("CPU");
            item->set_value("85%");

            item = req.add_items();
            item->set_key("MEM");
            item->set_value("75%");

       
            item = req.add_items();
            item->set_key("DISK");
            item->set_value("65%");

            // TODO FOR PRESENTATION
            //item = req.add_items();
            //item->set_key("I/O");
            //item->set_value("35%");

            Status status = stub_->Check(&ctx, req, &reply);
            if (status.ok()) {
                LOG(ERROR) << " HealtClient::Start() Metrics sent OK." << std::endl;
            } else {
                
                LOG(ERROR) << " HealtClient::Start()" << status.error_code() << ": " << status.error_message();

            
            }
            
            LOG(INFO) << "HealtClient::Start() sleep." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }

     }
  
}