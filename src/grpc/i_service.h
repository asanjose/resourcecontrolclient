#ifndef I_SERVICE_H
#define I_SERVICE_H

#include <memory>

namespace her
{
    class IGrpcService
    {
    public:
        virtual ~IGrpcService() = default;
        virtual bool Start() = 0;
        virtual bool Stop() = 0;
        virtual void Wait() = 0;
    };
}
#endif