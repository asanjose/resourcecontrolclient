#include "service_synchronous.h"
#include "log/aixlog.hpp"


namespace her{


        GrpcServiceSynchronous::GrpcServiceSynchronous(std::string url, int max_concurren_threads,
            std::vector<std::shared_ptr<grpc::Service>>  implemented_services,
            std::shared_ptr<grpc::ServerCredentials> server_credentials,
            std::vector<std::unique_ptr<grpc::experimental::ServerInterceptorFactoryInterface>> interceptor_creators)
            : 
            server_resource_quota(),
            service_url(url),
            services(implemented_services),
            credentials(server_credentials) {
            server_resource_quota.SetMaxThreads(max_concurren_threads);
            interceptors.reserve(interceptor_creators.size());
            interceptors = std::move(interceptor_creators);
        };

        bool GrpcServiceSynchronous::Stop() {
            try {
                server->Shutdown();
                return true;
            } catch (const std::exception& e) {
                return false;
            }
        }

        void GrpcServiceSynchronous::Wait() { server->Wait(); }

        bool GrpcServiceSynchronous::Start() {
            grpc::ServerBuilder builder;

            // Listen on the given address and  authentication mechanism.
            builder.AddListeningPort(service_url, credentials);

            // set max message size
            builder.SetMaxMessageSize(MAX_MESSAGE_SIZE);

            // resource quota
            builder.SetResourceQuota(server_resource_quota);

            // Register "service" as the instance through which we'll communicate with
            // clients. In this case it corresponds to an *synchronous* service.
            for(auto svc : services )
                builder.RegisterService(svc.get());

            // interceptor
            builder.experimental().SetInterceptorCreators(std::move(interceptors));

            // Finally assemble the server.
            server = std::move(builder.BuildAndStart());
            LOG(INFO) << "Server listening on " << service_url;
        }


}//her namepsace