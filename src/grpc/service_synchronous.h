#ifndef SERVICE_SYNCHRONOUS_HPP
#define SERVICE_SYNCHRONOUS_HPP

#include <memory>
#include "i_service.h"

// GRPC includes
#include <grpc++/grpc++.h>
#include <grpcpp/resource_quota.h>



namespace her
{
    // Max GRPC message size
    #define MAX_MESSAGE_SIZE 4 * 1024 * 1024
    
    class GrpcServiceSynchronous : public IGrpcService
    {
    public:
        GrpcServiceSynchronous(std::string url ,int max_concurren_threads,
            std::vector<std::shared_ptr<grpc::Service>>  implemented_services,
            std::shared_ptr<grpc::ServerCredentials> server_credentials,
            std::vector<std::unique_ptr<grpc::experimental::ServerInterceptorFactoryInterface>> interceptor_creators);

        bool Start();
        bool Stop();
        void Wait();

    private:
        
        std::string service_url;
        std::vector<std::shared_ptr<grpc::Service>> services;
        grpc::ResourceQuota server_resource_quota;
        std::unique_ptr<grpc::Server> server;
        std::vector<std::unique_ptr<grpc::experimental::ServerInterceptorFactoryInterface>> interceptors;
        std::shared_ptr<grpc::ServerCredentials> credentials;
    };

}  // namespace her
#endif